package com.sistemadepedidos.app;

import java.util.Arrays;

import com.sistemadepedidos.app.domain.Cidade;
import com.sistemadepedidos.app.domain.Estado;
import com.sistemadepedidos.app.repository.CidadeRepository;
import com.sistemadepedidos.app.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.sistemadepedidos.app.domain.Categoria;
import com.sistemadepedidos.app.domain.Produto;
import com.sistemadepedidos.app.repository.CategoriaRepository;
import com.sistemadepedidos.app.repository.ProdutoRepository;

@SpringBootApplication
public class AppApplication implements CommandLineRunner{

	@Autowired
	private CategoriaRepository categoriaRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;

	@Autowired
	private EstadoRepository estadoRepository;

	@Autowired
	private CidadeRepository cidadeRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(AppApplication.class, args);
	}

	//Método do CommandLineRunner, serve para que possamos instanciar objetos diretamente na aplicação.
	@Override
	public void run(String... args) throws Exception {
		
		Categoria cat1 = new Categoria(null, "Informática");
		Categoria cat2 = new Categoria(null, "Escritório");
		
		Produto p1 = new Produto(null, "Computador", 2000);
		Produto p2 = new Produto(null, "Impressora", 800);
		Produto p3 = new Produto(null, "Mouse", 80);
		
		cat1.getProdutos().addAll(Arrays.asList(p1, p2, p3));
		cat2.getProdutos().addAll(Arrays.asList(p2));
		
		p1.getCategorias().addAll(Arrays.asList(cat1));
		p2.getCategorias().addAll(Arrays.asList(cat1, cat2));
		p3.getCategorias().addAll(Arrays.asList(cat1));

		categoriaRepository.saveAll(Arrays.asList(cat1, cat2));
		produtoRepository.saveAll(Arrays.asList(p1, p2, p3));

		Estado est1 = new Estado("Paraíba");
		Estado est2 = new Estado("Pernambuco");

		Cidade c1 = new Cidade("Campina Grande", est1);
		Cidade c2 = new Cidade("Recife", est2);
		Cidade c3 = new Cidade("João Pessoa", est1);

		est1.getCidades().addAll(Arrays.asList(c1, c3));
		est2.getCidades().addAll(Arrays.asList(c2));
		
		estadoRepository.saveAll(Arrays.asList(est1, est2));
		cidadeRepository.saveAll(Arrays.asList(c1, c2, c3));
	}

}
