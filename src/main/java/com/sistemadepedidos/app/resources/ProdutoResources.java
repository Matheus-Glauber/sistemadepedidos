package com.sistemadepedidos.app.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sistemadepedidos.app.domain.Produto;
import com.sistemadepedidos.app.service.ProdutoService;

/**
 * Resource responsável pelo controle da Entidade Produto
 * @author glauber.jordao1995@gmail.com
 * @version 1.0
 * @since Release 1
 */
@RestController
@RequestMapping(value = "/produtos")
public class ProdutoResources{
	
	@Autowired
	private ProdutoService produtoService;
	
	/**
	 * Método que faz uma requisição GET para retornar um id da Entidade Produto.
	 * @param  id
	 * @return id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id){
		
		Produto obj = produtoService.buscar(id);
		 
		return ResponseEntity.ok().body(obj);
	}

}
