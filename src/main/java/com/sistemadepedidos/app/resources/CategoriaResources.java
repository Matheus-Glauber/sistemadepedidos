package com.sistemadepedidos.app.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sistemadepedidos.app.domain.Categoria;
import com.sistemadepedidos.app.service.CategoriaService;

/**
 * Resource responsável pelo controle da Entidade Categoria
 * @author glauber.jordao1995@gmail.com
 * @version 1.0
 * @since Release 1
 */
@RestController
@RequestMapping(value = "/categorias")
public class CategoriaResources {
	
	@Autowired
	private CategoriaService service;

	/**
	 * Método que faz uma requisição GET para retornar um id da entidade Categoria.
	 * @param  id
	 * @return id
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> find(@PathVariable Integer id) {
		
		Categoria obj = service.buscar(id);
		
		return ResponseEntity.ok().body(obj);
	}

}
