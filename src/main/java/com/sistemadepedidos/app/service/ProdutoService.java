package com.sistemadepedidos.app.service;

import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistemadepedidos.app.domain.Produto;
import com.sistemadepedidos.app.repository.ProdutoRepository;

/**
 * Service da Entidade Produto, responsável pelas regras de negócio da mesma.
 * @author glauber.jordao1995@gmail.com
 * @version 1.0
 * @since Release 1
 */
@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	/**
	 * Método para buscar um produto por id.
	 * @param id
	 * @return Integer id
	 */
	public Produto buscar(Integer id) {
		Optional<Produto> obj = produtoRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				9101356586139527058L, "Objeto não encontrado, ID: " + id + ", Tipo: " + Object.class.getName()));
	}

}
