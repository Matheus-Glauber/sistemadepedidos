package com.sistemadepedidos.app.service;

import java.util.Optional;

import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sistemadepedidos.app.domain.Categoria;
import com.sistemadepedidos.app.repository.CategoriaRepository;

/**
 * Service da Entidade Categoria, responsável pelas regras de negócio da mesma.
 * @author glauber.jordao1995@gmail.com
 * @version 1.0
 * @since Release 1
 */
@Service
public class CategoriaService {
	
	@Autowired //Dependência automaticamente instanciado pelo Spring, por injeção de dependência
	private CategoriaRepository repo;
	
	/**
	 * Método para buscar uma categoria por id.
	 * @param id
	 * @return Integer id
	 */
	public Categoria buscar(Integer id) {
		Optional<Categoria> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				3309591901613735987L, "Objeto não encontrado, ID: " + id + ", Tipo: " + Object.class.getName()));
	}

}
