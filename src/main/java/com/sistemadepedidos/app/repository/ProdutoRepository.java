package com.sistemadepedidos.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sistemadepedidos.app.domain.Produto;

/**
 * Responsável por acessar os dados do banco da Entidade Produto.
 * @author glauber.jordao1995@gmail.com
 * @version 1.0
 * @since Release 1
 */
@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer>{

}
