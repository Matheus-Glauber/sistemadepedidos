package com.sistemadepedidos.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sistemadepedidos.app.domain.Categoria;

/**
 * Responsável por acessar os dados do banco da Entidade Categoria.
 * @author glauber.jordao1995@gmail.com
 * @version 1.0
 * @since Release 1
 */
@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Integer> {

}
